===================================================
Lessons on machine learning for high energy physics
===================================================

This repository contains different lessons for machine learning using `scikit-learn <https://scikit-learn.org>`__.
You will need `numpy <https://numpy.org>`__, `pandas <https://pandas.pydata.org>`__, `matplotlib <https://matplotlib.org>`__ and `scikit-learn <https://scikit-learn.org>`__ on a Python3 environment in order to execute the notebooks.
To install the package simply run

.. code:: bash

   pip install -e .

in case you use a virtual environment (the *-e* flag allows you to install it using the current directory) or

.. code:: bash

   python setup.py install --prefix=$INSTALL_DIR

if you are using `Anaconda <https://www.anaconda.com>`__.

Execute the lessons
===================

Once you have installed the package, a script called *hep-lessons-ml* should become accessible.
Execute it by calling

.. code:: bash

   hep-lessons-ml

and it will open a Jupyter notebook with the available lessons. To close the application simply
press Ctrl+C or kill the associated process.

Supplementary scripts
=====================

You can find different scripts to automate the training and test process shown here for different classifiers and variables under the directory *scripts*.
The goal of each script is discussed here:

* *generate.py*: creates an HDF5 sample with the information of the particles from proton-proton collisions using Pythia8. You will need Pythia8 to be installed with python3 in order to execute it.
* *merge.py*: merge different files in HDF5 format into a single file.
* *combine.py*: in this script the combination of particles is done, preceded by a selection based on the impact parameter and transverse momenta of the tracks (to reduce the number of combinations to do). It is also possible to apply a preliminar selection on the combination of the particles.
* *classification.py*: allows to run over a sample generated using the previous scripts in order to study the sample, do the classification or compare the performance of different algorithms.

You can access the manual of each script by calling:

.. code:: bash

   ./<script>.py -h
