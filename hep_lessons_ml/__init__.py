import os

PACKAGE_PATH = os.path.abspath(os.path.dirname(__file__))

introduction_notebook = os.path.join(PACKAGE_PATH, 'notebooks', 'introduction.ipynb')

def sample_path(name):
    '''
    Get the path to a data sample by name.
    '''
    return os.path.join(PACKAGE_PATH, 'samples', name)
