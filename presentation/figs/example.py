#!/usr/bin/env/python
'''
Generate plots about the lessons.
'''
import argparse
import matplotlib.pyplot as plt
import numpy as np

from matplotlib.patches import ConnectionPatch


def generate_exp(p, a, b, n):
    '''
    Generate a sample distributed following an exponential.
    '''
    ip = 1. / p

    norm = ip * (np.exp(p * b) - np.exp(p * a))

    r = np.random.uniform(0, 1, n)

    return ip * np.log(norm * p * r + np.exp(p * a))


def plot_sig_bkg(sig, bkg, config, where=None):
    '''
    Plot a signal and a background sample on a stack histogram.
    '''
    where = where if where is not None else plt.gca()

    range = config.get('range', (0.4, 0.6))
    bins  = config.get('bins', 100)

    bkg_c, _ = np.histogram(bkg, range=range, bins=bins)
    sig_c, e = np.histogram(sig, range=range, bins=bins)

    c = 0.5 * (e[1:] + e[:-1])

    config['bins'] = e

    sig_c += bkg_c
    
    where.hist(c, weights=sig_c, label='sig', **config)
    where.hist(c, weights=bkg_c, label='bkg', **config)
    where.set_xlabel('invariant mass')


def main(eff, rej):

    sig = np.random.normal(0.497, 0.004, 1000)
    bkg = generate_exp(-5., 0.4, 0.6, 10000)

    whole = np.concatenate([sig, bkg])

    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(12, 4))

    common = dict(range=(0.4, 0.6), bins=100, histtype='step')
    
    plot_sig_bkg(sig, bkg, common, where=ax0)

    plot_sig_bkg(sig[:int(eff * len(sig))], bkg[:int((1 - rej) * len(bkg))], common, where=ax1)

    ax0.annotate('signal', (0.510, 200),)
    ax0.annotate('background', (0.420, 175))

    for ax in (ax0, ax1):
        ax.set_xlim(0.4, 0.6)

    ax1.legend()

    ax1loc = (0.480, 100)
    
    con = ConnectionPatch(xyA=ax1loc, xyB=(0.520, 150), coordsA='data', coordsB='data',
                          axesA=ax1, axesB=ax0, color='red', arrowstyle='<-', connectionstyle='arc3,rad=.1')

    ax1.annotate('after requirements', (0.85 * ax1loc[0], 1.2 * ax1loc[1]), color='red')

    ax1.add_artist(con)

    fig.patches.append(con)

    fig.savefig('sig_bkg_example.pdf')
    
    plt.show()
    

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--eff', default=0.9, help='Signal efficiency')
    parser.add_argument('--rej', default=0.9, help='Background rejection')

    args = parser.parse_args()
    
    main(**vars(args))
