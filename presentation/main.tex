%-------------------------------------------------------------------------------
% @author: Miguel Ramos Pernas
% @email:  miguel.ramos.pernas@gmail.com
% @date:   20/09/2016
%-------------------------------------------------------------------------------
%
% Description:
%
% Base to generate presentations using beamer.
%
%-------------------------------------------------------------------------------

\documentclass[8pt, xcolor=table]{beamer}

% Language
\usepackage[english]{babel}

% The style to be loaded
\usepackage{smartboa}
\usepackage{listings}
\usetikzlibrary{tikzmark,calc,shapes}

% Set the hyperlinks options
\hypersetup{
  colorlinks=true,
  linkcolor=violet,
  filecolor=blue,
  urlcolor=blue,
  citecolor=black,
}

% Packages to be used
\input{beamer_pkgs}

\newcommand{\scite}[2]{\href{#1}{\scriptsize[#2]}}

%-------------------------------------- TITLE

\Title{Machine Learning for event classification at LHCb}
\Authors{Miguel Ramos Pernas}
\Author{Miguel Ramos Pernas}
\Institute{Universidade de Santiago de Compostela}
\Email{miguel.ramos.pernas@cern.ch}
\Date{June 2, 2020}
\Logos{figs/logos.pdf}

% Change foot widths
\renewcommand{\footleftwidth}{0.3\paperwidth}
\renewcommand{\footmidwidth}{0.4\paperwidth}
\renewcommand{\footrightwidth}{0.3\paperwidth}

\definecolor{dgreen}{RGB}{0,150,0}

\newcommand{\tocite}{\textcolor{red}{TOCITE}}

\begin{document}

\parskip=0cm
\justify

%-------------------------------------- TITLE SLIDE
\begin{frame}[plain]
  \titlepage
\end{frame}
\parskip=0.2cm
\clearpage
\pagebreak

\begin{frame}{Introduction}
  \begin{tikzpicture}
    \node (fig) {\includegraphics[width=0.4\textwidth]{figs/kxdc-machine-learning.png}};
    \node at (fig.south east) [anchor=north east] {
      \href{https://xkcd.com/1838}{\small https://xkcd.com/1838}
    };
    \node (text) at (fig.west) [anchor=east,text width=0.5\textwidth,xshift=-0.5cm] {
      \begin{minipage}{\textwidth}
        Just because I love disclaimers:
        
        \begin{sitemize}[sitemsep=0.2cm]
        \item I am not a data scientist (I am a particle physicist).
        \item This lesson and the challenges that are proposed (including the way to solve them) arise from the knowledge that I have acquired through the years.
        \item All the material that you find will here has been simplified (real life tends to be more complicated).
        \item The challenges are specific applications of Machine Learning (ML) to particle physics.
        \end{sitemize}
      \end{minipage}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}{Implementing ML algorithms is easier than ever!}
  \centering
  \begin{tikzpicture}
    \node (market) {\includegraphics[width=0.8\textwidth]{figs/machine-learning-market.png}};
    \node (tmva) at (market.north east) [overlay,anchor=north east,xshift=0.5cm,yshift=0.1cm,text width=0.2\textwidth] {
      \includegraphics[width=\textwidth]{figs/tmva.png}
      I actually started using this!
    };
    \node (text) at (market.south) [anchor=north,text width=\textwidth] {
      \begin{minipage}{\textwidth}
        \begin{sitemize}[sitemsep=0.2cm]
        \item A lot of options available for free, many of them with a friendly python interface.
        \item In this case we will use the \href{https://scikit-learn.org}{scikit-learn} package, but you are free to implement algorithms using any of the other packages.
        \end{sitemize}
      \end{minipage}
    };
  \end{tikzpicture}
  
\end{frame}

\begin{frame}{What I have used so far...}
  \centering
  \begin{tikzpicture}
    \node (fig) {\includegraphics[width=0.9\textwidth]{figs/machine-learning.jpg}};
    \draw [color=red,line width=2pt] (-0.1, 2.5) -- (2, -1) -- (5, -1) -- (6, 1) -- (5, 4) -- (1, 4) -- (-0.1, 3.5) -- cycle;
    \node at (fig.south east) [anchor=south east,draw=red,rounded corners,text width=0.3\textwidth,xshift=0.5cm,yshift=1.25cm] {
      Classification for background rejection and regressions to improve data/simulation agreement.
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}{A common task to do in particle physics}
  \begin{tikzpicture}
    \node (fig) {\includegraphics[width=\textwidth]{figs/sig_bkg_example.pdf}};
    \node at (fig.south) [anchor=north,yshift=-0.2cm,text width=\textwidth]{
      \begin{minipage}{\textwidth}
        \begin{sitemize}[sitemsep=0.3cm,margin=8pt]
        \item Many times in particle physics we want to apply a selection to remove background whilst keeping the highest signal efficiency.
        \item Rectangular requirements, like \(p_T > 250~\text{MeV}/c\) have worse performance than selections based on ML classifiers.
        \item Despite many times the problem to solve is similar: keep the signal peak as high as possible over a background distribution, the choice of ML classifier and training procedure varies.
        \end{sitemize}
      \end{minipage}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}{Into the lessons}
  \centering
  \begin{tikzpicture}
    \node (fig) {\includegraphics[width=\textwidth]{figs/event-display.png}};
    \node at (fig.south west) [overlay,fill=white,anchor=south west,text width=0.8\textwidth,yshift=-0.5cm] {
      \begin{minipage}{\textwidth}
          \begin{sitemize}
          \item Examples are based on simulated and real data proton-proton collisions.
          \item All data samples are composed by combinations of two oppositely charged particles (forming a candidate).
          \item The challenge relies on identifying a signal decay (for example, \(K_\mathrm{S}^0\rightarrow\pi^+\pi^-\)) in a data sample collected by an experiment.
          \item The identification must be done training a classifier based on topological and kinematic information of the candidates.
          \end{sitemize}
      \end{minipage}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}{Downloading the lessons}
  \centering
  \begin{tikzpicture}
    \node (fig) {\includegraphics[width=\textwidth]{figs/download.jpg}};
      \node (text) at (fig.north) [yshift=0.2cm] {
        Start by going to \href{https://gitlab.cern.ch/mramospe/hep-lessons-ml}{this URL}
      };
      \node (download) at (fig.east) [circle,draw=red,line width=1.5pt,yshift=0.5cm,xshift=-1.9cm,minimum width=1cm] { };
      \node (download text)at (download.south west) [fill=white,line width=1.5pt,draw=red,anchor=north east,xshift=-1cm,yshift=-0.5cm,text width=0.3\textwidth] {
        Download the package and decompress it in your favorite directory.
      };
      \draw [-latex,red,line width=1.5pt] (download text.north east) -- (download.south west);
  \end{tikzpicture}
\end{frame}

\lstset{language=bash,
numbers=left,
breaklines=true,
basicstyle=\ttfamily\small,
showspaces=false,                
showstringspaces=false,
showtabs=false,
otherkeywords={pip,virtualenv,python},
keywordstyle=\color{dgreen},
commentstyle=\color{red},
frame=leftline,
}

\begin{frame}[fragile]{Installing the package}

  The lessons are given as a standard python package.
  You will need a Python version \(\geq 3.6\) installed in your system to execute the notebooks.
  I personally prefer to use \href{https://docs.python.org/3/tutorial/venv.html}{virtual environments} (calling \texttt{virtualenv}).

  The lessons and the dependencies on other python packages can be installed using \texttt{pip}
  \begin{lstlisting}[language=bash]
    pip install -e ``path to hep_lessons_ml''
  \end{lstlisting}
  The \texttt{-e} will allow you to install the package in-place.

  For \href{https://www.anaconda.com}{anaconda} users, you can install it calling
  \begin{lstlisting}[language=bash]
    python ``path to hep_lessons_ml''/setup.py install --prefix=$INSTALL_DIR
  \end{lstlisting}
  Installing the package might take some time \(5-10~\text{min}\), since in addition to the package dependencies different data samples will be downloaded.
  \vspace{0.2cm}
  
  You can access the data sets in CERNBox:\vspace{-0.1cm}
  \begin{sitemize}[margin=8pt,sitemsep=0.2cm]
  \item Pythia8 proton-proton collisions: \href{https://cernbox.cern.ch/index.php/s/ITytZAD7mMneNj1}{https://cernbox.cern.ch/index.php/s/ITytZAD7mMneNj1}.
  \item LHCb data: \href{https://cernbox.cern.ch/index.php/s/ys319P8XQp6vp5R}{https://cernbox.cern.ch/index.php/s/ys319P8XQp6vp5R}.
  \item \(K_\mathrm{S}^0 \rightarrow \pi^+\pi^-\) MC: \href{https://cernbox.cern.ch/index.php/s/fqebtiOyVBH6OyN}{https://cernbox.cern.ch/index.php/s/fqebtiOyVBH6OyN}.
  \item \(K_\mathrm{S}^0 \rightarrow \pi\mu\nu\) MC: \href{https://cernbox.cern.ch/index.php/s/MIdUb2Z0AuGMLSc}{https://cernbox.cern.ch/index.php/s/MIdUb2Z0AuGMLSc}.
  \end{sitemize}
  
\end{frame}

\begin{frame}[fragile]{Start the lessons}
  \centering
  \begin{tikzpicture}
    \node (text) [text width=0.5\textwidth]{
      \begin{minipage}{\textwidth}
        \begin{sitemize}[sitemsep=0.3cm,margin=8pt]
        \item Once you have installed the package, you should be able to start the lessons by invoking \texttt{hep-lessons-ml} (it might be in \texttt{\(\sim\)/.local/bin}).
        \item This will open a notebook where you can find the links to the challenges.
        \item You are free to reuse the code you see here for your own purposes.
        \item Remember that the application of ML algorithms is case-dependent
        \item I can give advises about how to tackle particular problems; but if you need to deeply understand and implement an algorithm for a very specific case, better ask a data scientist!
        \end{sitemize}
      \end{minipage}
    };
    \node (fig) at (text.east) [anchor=west,xshift=0.5cm] {\includegraphics[width=0.4\textwidth]{figs/call-data-scientist.jpg}};
  \end{tikzpicture}

\end{frame}

\end{document}
