#!/usr/bin/env python
'''
Classification problem to select KS0 -> pi+ pi- samples from a general simulated
sample.
'''
import argparse
import json
import logging
import matplotlib.pyplot as plt
import numpy as np
import pandas

# Load classifiers
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.metrics import roc_auc_score
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier


logger = logging.getLogger()

np.random.seed(4857)


CLASSIFIERS = {cl.__class__.__name__: cl for cl in (
    KNeighborsClassifier(n_neighbors=100),
    SVC(kernel='linear', C=0.025, probability=True),
    GaussianProcessClassifier(1.0 * RBF(1.0)),
    DecisionTreeClassifier(max_depth=10, min_samples_leaf=0.1,
                           max_features='auto', min_samples_split=4),
    RandomForestClassifier(max_depth=5, n_estimators=100),
    MLPClassifier(alpha=1, max_iter=1000),
    AdaBoostClassifier(base_estimator=DecisionTreeClassifier(
        max_depth=5, min_samples_leaf=0.1, max_features='auto'), learning_rate=0.2, n_estimators=50),
    GaussianNB(),
    QuadraticDiscriminantAnalysis()
)}

PID_VARS = {'p1_pid', 'p2_pid', 'p1_head_rid',
            'p2_head_rid', 'p1_head_pid', 'p2_head_pid'}


def chunks(lst, n):
    '''
    Divide a collection in chunks of size "n".
    '''
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def prepare_sample(path, variables, make_variables):
    '''
    Create an input sample and calculate additional variables if
    requested.
    '''
    load_variables = set(variables).union(PID_VARS)

    math_dict = {
        'exp': np.exp,
        'log': np.log,
        'min': np.minimum,
        'max': np.maximum,
        'sqrt': np.sqrt,
    }

    for v, expr in make_variables.items():

        c = compile(expr, '<string>', 'eval')

        for _, e in reversed(sorted(zip(map(len, c.co_names), c.co_names))):
            if e == '_df':
                raise ValueError('Expression must not contain the name "_df"')
            if not e in math_dict:
                load_variables.add(e)
                expr = expr.replace(e, f'_df["{e}"]')

        make_variables[v] = expr

    df = pandas.read_hdf(path, usecols=list(load_variables), key='root')

    math_dict['_df'] = df

    for v, op in make_variables.items():
        df.loc[:, v] = eval(expr, {'__builtins__': {}}, math_dict)

    new_vars = list(set(variables).union(make_variables.keys()))

    return df, new_vars


def signal_mask(sample):
    '''
    Get the KS0 -> pi+ pi- component from a simulated sample. It returns a mask
    array to be applied to a sample to select such component.
    '''
    pid_cond = np.logical_and(
        sample['p1_pid'] == +211, sample['p2_pid'] == -211)  # two pions with opposite sign
    head_cond = np.logical_and(sample['p1_head_rid'] == sample['p2_head_rid'],
                               sample['p1_head_pid'] == 310)  # common head (a KS0)
    return np.logical_and(pid_cond, head_cond)


def classify(sample, train_test_ratio, classifiers, variables, ofile, make_variables):
    '''
    Do a classification using a training sample
    '''
    variables = list(variables)  # for data frame indexing

    if classifiers is None:
        classifiers = list(CLASSIFIERS.keys())

    logger.info('Loading input sample')

    smp, variables = prepare_sample(sample, variables, make_variables)

    # reduce the invariant mass window
    smp = smp[np.logical_and(smp['head_m'] >= 0.4, smp['head_m'] < 0.6)]

    mask = signal_mask(smp)

    smp.loc[:, 'signal'] = mask

    # Split in train/test
    train_mask = np.full(len(smp), False, dtype=np.bool)
    train_mask[:int(round(train_test_ratio * len(smp) /
                          (1 + train_test_ratio)))] = True

    np.random.shuffle(train_mask)

    smp.loc[:, 'train'] = train_mask

    # Sample for training
    train = smp[smp['train']]

    logger.info(
        f'Training sample has {len(train)} candidates (signal = {np.count_nonzero(train["signal"])})')

    for c in classifiers:
        cl = CLASSIFIERS[c]
        logger.info(f'- {cl.__class__.__name__}')
        cl.fit(train[variables], train['signal'])

    # Apply classifiers to train and test samples

    logger.info('Applying the classifiers to the output sample')

    for c in classifiers:
        cl = CLASSIFIERS[c]
        smp.loc[:, cl.__class__.__name__] = cl.predict_proba(
            smp[variables])[:, 1]  # 0 for background

    smp.to_hdf(ofile, key='root', index=False)


def describe(sample, variables, ranges, make_variables):
    '''
    Plot the distribution of the variables in the given sample, their
    correlation and show their importance for a classification problem.
    '''
    logger.info('Loading sample')

    df, variables = prepare_sample(sample, variables, make_variables)

    mask = signal_mask(df)

    sig, bkg = df[mask], df[~mask]

    # Plot using axes on a 2 x 4 grid
    logger.info('Plotting variables')

    for vs in chunks(variables, 8):

        fig, axs = plt.subplots(2, 4, figsize=(18, 6))

        axs = axs.flatten()

        for v, ax in zip(vs, axs):
            logger.info(f'- {v}')
            vr = ranges.get(v, (df[v].min(), df[v].max()))
            ax.hist(bkg[v], range=vr, bins=100, color='C3',
                    alpha=0.5, density=True, label='bkg')
            ax.hist(sig[v], range=vr, bins=100, color='C0',
                    histtype='step', density=True, label='sig')
            ax.set_xlabel(v)
            ax.legend()

        for i in range(len(axs) - 1, len(vs) - 1, -1):
            axs[i].set_axis_off()

        fig.tight_layout()

    # Do the correlation histograms for signal and background
    logger.info('Calculating the correlation for signal and background proxies')

    sig_corr = sig[variables].corr()

    logger.info(f'Correlations for signal\n{sig_corr}')

    bkg_corr = sig[variables].corr()

    logger.info(f'Correlations for background\n{bkg_corr}')

    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(16, 6))

    for corr, ax in ((sig_corr, ax0), (bkg_corr, ax1)):

        edges = np.arange(1, len(corr.columns) + 2)

        centers = 0.5 * (edges[1:] + edges[:-1])

        x, y = tuple(a.flatten() for a in np.meshgrid(centers, centers))

        _, _, _, obj = ax.hist2d(x, y, bins=(
            edges, edges), weights=corr.values.flatten(), alpha=0.4, cmap='inferno')

        for ci, v1 in zip(centers, variables):
            for cj, v2 in zip(centers, variables):
                ax.text(
                    ci, cj, f'{corr.loc[v1, v2]:.4f}', ha='center', va='center')

        ax.set_xticks(centers)
        ax.set_xticklabels(variables)
        ax.set_yticks(centers)
        ax.set_yticklabels(variables)

    fig.tight_layout()

    # Rank the provided variables
    logger.info('Calculating the ranking of variables')

    clf = DecisionTreeClassifier()
    clf.fit(df[variables], mask)

    importances, sorted_variables = zip(
        *sorted(zip(clf.feature_importances_, variables)))

    ranking = pandas.Series(importances, index=sorted_variables)

    logger.info(f'Ranking of variables:\n{ranking}')

    yticks = np.arange(1, len(importances) + 1)

    fig, ax = plt.subplots(1, 1, figsize=(6, 4))

    for i, im in zip(yticks, importances):
        ax.barh(i, im, height=0.5, color='firebrick')

    ax.set_yticks(yticks)
    ax.set_yticklabels(sorted_variables)
    ax.set_xlabel('importance')
    ax.grid()

    fig.tight_layout()

    plt.show()


def results(ifile, classifiers, ranges):
    '''
    Show the results of the classification process.
    '''
    classifiers = np.array(
        classifiers if classifiers is not None else list(CLASSIFIERS.keys()))

    whole = pandas.read_hdf(ifile, key='root', usecols=list(
        classifiers) + ['signal', 'train'])

    train = whole[whole['train']]
    test = whole[~whole['train']]

    train_sig, train_bkg = train[train['signal']], train[~train['signal']]
    test_sig, test_bkg = test[test['signal']], test[~test['signal']]

    # Distribution of training and test samples (overtraining check)
    for cs in chunks(classifiers, 8):

        fig, axs = plt.subplots(2, 4, figsize=(18, 6))

        axs = axs.flatten()

        for c, ax in zip(cs, axs.flatten()):
            common = dict(range=ranges.get(c, (0, 1)), bins=100, density=True)
            ax.hist(train_bkg[c], histtype='stepfilled',
                    color='C3', label='train bkg', alpha=0.5, **common)
            ax.hist(train_sig[c], histtype='stepfilled',
                    color='C0', label='train sig', alpha=0.5, **common)
            ax.hist(test_bkg[c], histtype='step',
                    color='C3', label='test bkg', **common)
            ax.hist(test_sig[c], histtype='step',
                    color='C0', label='test sig', linestyle='--', **common)
            ax.set_xlabel(c)
            ax.legend()

        for i in range(len(axs) - 1, len(cs) - 1, -1):
            axs[i].set_axis_off()

        fig.tight_layout()

    # ROC curves
    fig, ax = plt.subplots(1, 1, figsize=(10, 7))

    probs = np.linspace(0, 1, 1000)

    roc_scores = np.fromiter(
        (roc_auc_score(test['signal'], test[c]) for c in classifiers), dtype=np.float)

    order = np.argsort(roc_scores)[::-1]

    for i, (c, r) in enumerate(zip(classifiers[order], roc_scores[order])):
        eff = 1. - np.searchsorted(np.sort(test_sig[c]), probs) / len(test_sig)
        rej = np.searchsorted(np.sort(test_bkg[c]), probs) / len(test_bkg)
        ax.plot(rej, eff, label=f'{c} (auc = {r:.4f})', zorder=-i)

    ax.legend()
    ax.set_xlim(0, 1)
    ax.set_xlabel('background rejection')
    ax.set_ylim(0, 1)
    ax.set_ylabel('signal efficiency')

    plt.show()


if __name__ == '__main__':

    logging.basicConfig(
        format='%(levelname)s: %(message)s', level=logging.INFO)

    # Define the options
    parser = argparse.ArgumentParser(description=__doc__)
    subparsers = parser.add_subparsers(description='Mode to run')

    des = subparsers.add_parser(describe.__name__, help=describe.__doc__)
    des.set_defaults(function=describe)
    des.add_argument('sample', type=str, help='Sample to process')

    cfy = subparsers.add_parser(classify.__name__, help=classify.__doc__)
    cfy.set_defaults(function=classify)
    cfy.add_argument('sample', type=str, help='Sample to use')
    cfy.add_argument('--train-test-ratio', type=float, default=1.,
                     help='Ratio of candidates used to train and to test the classifiers.')

    cfy.add_argument('--ofile', type=str, default='whole_sample.hdf5',
                     help='Output file of the classification process, containing the train and test samples with the evaluation of the classification methods.')

    for p in des, cfy:
        p.add_argument('--variables', nargs='*', default=('head_doca', 'head_ip', 'p1_ip', 'p2_ip',
                                                          'head_pt', 'p1_pt', 'p2_pt'), help='Variables to use in the classification process')
        p.add_argument('--make-variables', type=json.loads,
                       default='{}', help='Additional variables to construct')

    res = subparsers.add_parser(results.__name__, help=results.__doc__)
    res.set_defaults(function=results)
    res.add_argument(
        'ifile', type=str, help='File with the information from the classification process')

    for p in cfy, res:
        p.add_argument('--classifiers', nargs='+', default=['KNeighborsClassifier',
                                                            'DecisionTreeClassifier',
                                                            'AdaBoostClassifier',
                                                            'MLPClassifier',
                                                            'GaussianNB',
                                                            'QuadraticDiscriminantAnalysis'],
                       help='Classifiers to process')

    for p in des, res:
        p.add_argument('--ranges', type=json.loads, default='{}',
                       help='Range for the variables to display. Must be given as a python dictionary: \'{"varname": (0.4, 0.6)}\'')

    # Parser the arguments
    args = parser.parse_args()

    cfg = vars(args)

    func = cfg.pop('function')

    func(**cfg)
