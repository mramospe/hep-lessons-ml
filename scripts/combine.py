#!/usr/bin/env python
'''
Combine particles on a data sample using the given descriptor and write
the information on an output file.
'''
import argparse
import numpy as np
import pandas

np.random.seed(4587)


def xyz(x, y, z):
    return np.array([x, y, z], dtype=float)


def cross(v1, v2):
    x = v1[1] * v2[2] - v1[2] * v2[1]
    y = v1[2] * v2[0] - v1[0] * v2[2]
    z = v1[0] * v2[1] - v1[1] * v2[0]
    return xyz(x, y, z)


def dot(v1, v2):
    return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]


def mod(v):
    return np.sqrt(dot(v, v))


def unitary(v):
    m = mod(v)
    return xyz(v[0] / m, v[1] / m, v[2] / m)


def distance_to_point(p, v, momenta):
    '''
    Calculate the distance to a point using the momenta and the vertex.
    '''
    ap = p.reshape(3, 1) - v
    u = unitary(momenta)
    dv = cross(ap, u)
    return mod(dv)


def distance_two_lines(p1, v1, p2, v2):
    '''
    Calculate the distance between two lines defined from two points and two
    vectors.
    '''
    a12 = p2 - p1
    c12 = cross(v1, v2)
    if np.allclose(c12, 0.):  # parallel
        return np.sqrt(mod(a12)**2 - dot(a12, unitary(v1))**2)
    else:
        return np.abs(dot(a12, c12)) / mod(c12)


def middle_point(p1, v1, p2, v2):
    '''
    Function to be minimized in order to determine the closest point to two lines.
    '''
    a = dot(v1, v2)
    p = p2 - p1
    t = (dot(p, v1) - a * dot(p, v2)) / (1. - a * a)
    s = (a * dot(p, v1) - dot(p, v2)) / (1. - a * a)
    return 0.5 * (p2 + s * v2 + p1 + t * v1)


def combine(pos, neg, xyres, zres, momres, min_ip, max_doca, max_head_ip):
    '''
    Combine two particles, smearing some variables to account for the resolution
    of the detector.
    '''
    df = pandas.DataFrame(index=pos.index)

    pv = xyz(
        np.random.normal(0, xyres * 1e-3),
        np.random.normal(0, xyres * 1e-3),
        np.random.normal(0, zres * 1e-3),
    )

    # Apply the resolution and determine the momenta of the head particle
    for i, c in enumerate(['x', 'y', 'z']):

        df.loc[:, f'pv_{c}'] = pv[i]

        dres = (zres if c == 'z' else xyres) * \
            1e-3  # from micrometers to millimiters

        for p, smp in (('p1', pos), ('p2', neg)):
            df.loc[:, f'{p}_true_p{c}'] = smp[f'p{c}']
            df.loc[:, f'{p}_p{c}'] = np.random.normal(
                smp[f'p{c}'], momres * np.abs(smp[f'p{c}']))
            df.loc[:, f'{p}_true_ownpv_{c}'] = smp[f'ownpv_{c}']
            df.loc[:, f'{p}_vst_{c}'] = np.random.normal(
                smp[f'ownpv_{c}'], dres)
            df.loc[:, f'{p}_true_sv_{c}'] = smp[f'sv_{c}']
            df.loc[:, f'{p}_sv_{c}'] = np.random.normal(
                smp[f'sv_{c}'], dres)

        df.loc[:, f'head_p{c}'] = df[f'p1_p{c}'] + df[f'p2_p{c}']

    # Fill particle-related variables
    for v in 'pid', 'rid', 'head_pid', 'head_rid':
        df.loc[:, f'p1_{v}'] = pos[v]
        df.loc[:, f'p2_{v}'] = neg[v]

    df.loc[:, 'p1_true_m'] = pos['m']
    df.loc[:, 'p2_true_m'] = neg['m']

    df.loc[:, 'p1_m'] = np.full(len(df), 0.13957061)  # mass of the pi+ in GeV
    df.loc[:, 'p2_m'] = np.full(len(df), 0.13957061)  # mass of the pi- in GeV

    # Fill kinematic variables
    for p in 'p1', 'p2', 'head':
        df.loc[:, f'{p}_pt'] = np.sqrt(df[f'{p}_px']**2 + df[f'{p}_py']**2)
        df.loc[:, f'{p}_p'] = np.sqrt(df[f'{p}_pt']**2 + df[f'{p}_pz']**2)

        if p != 'head':
            df.loc[:, f'{p}_e'] = np.sqrt(df[f'{p}_p']**2 + df[f'{p}_m']**2)
            df.loc[:, f'{p}_true_pt'] = np.sqrt(
                df[f'{p}_true_px']**2 + df[f'{p}_true_py']**2)
            df.loc[:, f'{p}_true_p'] = np.sqrt(
                df[f'{p}_true_pt']**2 + df[f'{p}_true_pz']**2)
            df.loc[:, f'{p}_true_e'] = np.sqrt(
                df[f'{p}_true_p']**2 + df[f'{p}_m']**2)

    df.loc[:, 'head_e'] = df['p1_e'] + df['p2_e']
    df.loc[:, 'head_m'] = np.sqrt(df['head_e']**2 - df['head_p']**2)

    # Fill with vertex-related quantities
    p1 = xyz(df['p1_vst_x'], df['p1_vst_y'], df['p1_vst_z'])
    m1 = xyz(df['p1_px'], df['p1_py'], df['p1_pz'])
    p2 = xyz(df['p2_vst_x'], df['p2_vst_y'], df['p2_vst_z'])
    m2 = xyz(df['p2_px'], df['p2_py'], df['p2_pz'])

    df.loc[:, 'head_sv_x'], df.loc[:, 'head_sv_y'], df.loc[:,
                                                           'head_sv_z'] = middle_point(p1, m1, p2, m2)

    for p in 'p1', 'p2', 'head':
        sv = xyz(df[f'{p}_sv_x'], df[f'{p}_sv_y'], df[f'{p}_sv_z'])
        m = xyz(df[f'{p}_px'], df[f'{p}_py'], df[f'{p}_pz'])
        df.loc[:, f'{p}_ip'] = distance_to_point(pv, sv, m)
        df.loc[:, f'{p}_fd'] = mod(sv - pv.reshape(3, 1))
        df.loc[:, f'{p}_ctau'] = df[f'{p}_fd'] * df[f'{p}_m'] / df[f'{p}_p']

    df.loc[:, 'head_doca'] = distance_two_lines(p1, m1, p2, m2)

    # Filter the combinations
    df = df[(df['head_doca'] < max_doca) & (df['head_ip'] < max_head_ip)]

    return df  # data frame for a single event


def main(sample, max_fdz, min_pt, ofile, momres, xyres, zres, min_ip, max_doca, max_head_ip):

    df = pandas.read_hdf(sample, key='root').set_index(
        'event')  # load by event number

    # Calculate the IP to filter by it
    p = xyz(df['sv_x'], df['sv_y'], df['sv_z'])
    v = xyz(df['px'], df['py'], df['pz'])
    df.loc[:, 'ip'] = distance_to_point(xyz(0, 0, 0), p, v)

    # Do the combinations
    positive = df[(df['charge'] > 0) & (df['pt'] > min_pt) &
                  (df['sv_z'] < max_fdz) & (df['ip'] > min_ip)]
    negative = df[(df['charge'] < 0) & (df['pt'] > min_pt) &
                  (df['sv_z'] < max_fdz) & (df['ip'] > min_ip)]

    # Iterate event by event to reduce memory usage
    out = None

    cid = 0
    for i in np.unique(df.index):

        pos = positive[positive.index == i].reset_index()
        neg = negative[negative.index == i].reset_index()

        if not len(pos) or not len(neg):
            continue

        pos_idx, neg_idx = tuple(a.flatten() for a in np.meshgrid(
            np.arange(len(pos)), np.arange(len(neg))))

        pos_comb, neg_comb = pos.take(pos_idx), neg.take(neg_idx)

        pos_comb = pos_comb.reset_index()
        neg_comb = neg_comb.reset_index()

        # do the combination
        comb = combine(pos_comb, neg_comb, xyres, zres,
                       momres, min_ip, max_doca, max_head_ip)
        comb.loc[:, 'event'] = np.full(len(comb), i, dtype=np.int64)
        comb.loc[:, 'cid'] = np.arange(cid, cid + len(comb), dtype=np.int64)
        comb.loc[:, 'in_event_cid'] = np.arange(len(comb), dtype=np.int64)

        comb = comb.set_index('cid')

        if out is None:
            out = comb
        else:
            out = out.append(comb)

        cid += len(comb)  # increase the candidate id

    out.to_hdf(ofile, index=False, key='root')  # save the data frame


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('sample', type=str, help='Data sample to use')
    parser.add_argument('--min-pt', type=float, default=0.1,
                        help='Minimum transverse momentum of the decay products (in GeV)')
    parser.add_argument('--min-ip', type=float, default=0.5,
                        help='Minimum impact parameter of the decay products (in mm)')
    parser.add_argument('--max-fdz', type=float, default=1000.,
                        help='Minimum flight distance in the Z direction (in mm)')
    parser.add_argument('--max-doca', type=float, default=5.,
                        help='Minimum distance between two tracks to form a candidate (in mm)')
    parser.add_argument('--max-head-ip', type=float, default=1000.,
                        help='Maximum impact parameter for the head particle')
    parser.add_argument('--xyres', type=float, default=50.,
                        help='Resolution of distances in X or Y (in micrometers)')
    parser.add_argument('--zres', type=float, default=200.,
                        help='Resolution of distances in Z (in micrometers)')
    parser.add_argument('--momres', type=float, default=0.01,
                        help='Momentumm resolution applied to each coordinate')
    parser.add_argument(
        '--ofile', type=str, default='reconstructed.hdf5', help='Path to the output file')
    args = parser.parse_args()
    main(**vars(args))
