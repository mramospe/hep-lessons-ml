#!/usr/bin/env python
'''
Generate flavour production plots as a function of energy
'''

# Python
import argparse
import itertools
import math
import numpy as np
import pandas

# Pythia
from pythia8 import Pythia


def fill_sample_deco(*names):
    '''
    Create a decorator which stores the names of the variables that
    will be used to fill a sample.
    '''
    class wrapper(object):

        def __init__(self, function):
            '''
            Wrap the function to call.
            '''
            super().__init__()
            self.__function = function

        def __call__(self, prt):
            '''
            Fill a data frame with values from a particle.
            '''
            dct = self.__function(prt)
            if len(set(self.names).difference(dct.keys())):
                raise RuntimeError(
                    f'Variables and column names do not match for {self.__function.__name__}')
            return dct

    wrapper.names = names

    return wrapper


@fill_sample_deco('pid', 'head_rid', 'charge', 'three_charge')
def fill_particle_properties(prt):
    if prt.mother1() > 0 and prt.mother2() == 0:
        head_rid = prt.mother1()
    else:
        head_rid = -1
    return dict(pid=prt.id(), head_rid=head_rid, charge=prt.charge(), three_charge=prt.chargeType())


@fill_sample_deco('m', 'px', 'py', 'pz', 'p', 'pt', 'e')
def fill_kinematic(prt):
    pt = math.sqrt(prt.px()**2 + prt.py()**2)
    p = math.sqrt(pt**2 + prt.pz()**2)
    return dict(
        m=prt.m(), px=prt.px(), py=prt.py(), pz=prt.pz(), p=p, pt=pt, e=prt.e())


@fill_sample_deco('has_decay_head', 'ownpv_x', 'ownpv_y', 'ownpv_z', 'sv_x', 'sv_y', 'sv_z')
def fill_vertices(prt):
    '''
    A particle is considered to be a decay product if, in Pythia mother1 > 0 and mother2 = 0.
    '''
    return dict(
        has_decay_head=(prt.mother1() > 0 and prt.mother2() == 0),
        ownpv_valid=prt.hasVertex(),
        ownpv_x=prt.xProd(),
        ownpv_y=prt.yProd(),
        ownpv_z=prt.zProd(),
        sv_x=prt.xDec(),
        sv_y=prt.yDec(),
        sv_z=prt.zDec(),
    )


FUNCTIONS = [fill_particle_properties, fill_kinematic, fill_vertices]

ALL_VARS = list(itertools.chain.from_iterable(f.names for f in FUNCTIONS))


def main(energy, nevts, seed, ofile):
    '''
    Main function to call
    '''
    pythia = Pythia()
    # 1) Settings used in the main program.
    pythia.readString('Random:setSeed = on')
    pythia.readString(f'Random:seed = {seed}')
    pythia.readString('HardQCD:all = on')

    pythia.readString(f'Main:numberOfEvents = {nevts}')
    pythia.readString('Main:timesAllowErrors = 3')

    # 2) Settings related to output in init(), next() and stat().
    # list changed settings
    pythia.readString('Init:showChangedSettings = on')
    # list changed particle data
    pythia.readString('Init:showChangedParticleData = off')

    # print message every n events
    pythia.readString('Next:numberCount = 100')
    # print event information n times
    pythia.readString('Next:numberShowInfo = 1')
    # print process record n times
    pythia.readString('Next:numberShowProcess = 1')
    # print event record n times
    pythia.readString('Next:numberShowEvent = 10')

    # 3) Beam parameter settings. Values below agree with default ones.
    # first beam, p = 2212, pbar = -2212
    pythia.readString('Beams:idA = 2212')
    # second beam, p = 2212, pbar = -2212
    pythia.readString('Beams:idB = 2212')
    pythia.readString('Beams:eCM = {}'.format(
        energy))  # CM energy of collision

    # Iterate over all the events searching for the generated particles
    pythia.init()
    registry = {}
    for i in range(nevts):
        if not pythia.next():
            continue

        all_particles = list(pythia.event)  # to get the correct head reference

        particles = list(filter(lambda p: p.isVisible() and (
            p.isHadron() or p.isLepton()), pythia.event))

        evt = np.empty(len(particles), dtype=[('event', int), ('rid', int), ('head_pid', int)] + [
                       ((v, int) if v in ('pid', 'head_rid') else (v, float)) for v in ALL_VARS])
        for j, prt in enumerate(particles):

            head_pid = all_particles[prt.mother1()].id(
            ) if prt.mother1() > 0 and prt.mother2() == 0 else 0

            dct = {'event': i, 'rid': j, 'head_pid': head_pid}
            for f in FUNCTIONS:
                dct.update(f(prt))

            evt[j] = (dct['event'], dct['rid'], dct['head_pid']) + \
                tuple(dct[v] for v in ALL_VARS)

        registry[i] = pandas.DataFrame.from_records(evt)

    # The index of the DataFrame is the event number
    registry = pandas.concat(registry, ignore_index=True)

    # Save the output to a CSV file
    registry.to_hdf(ofile.format(
        energy=energy, seed=seed, nevts=nevts), key='root', index=False)


if __name__ == '__main__':

    # Build and parse command line arguments
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--energy', type=float,
                        default=14e3,
                        help='Energy of the collisions in GeV'
                        )
    parser.add_argument('--nevts', type=int,
                        default=100,
                        help='Number of events to process'
                        )
    parser.add_argument('--seed', type=int, default=0, help='Seed for Pythia')
    parser.add_argument('--ofile', type=str,
                        # a compressed CSV file
                        default='sample_{energy}_{seed}_{nevts}.hdf5',
                        help='Output file to write the information in. It allows to set placeholders for the other arguments passed to the script: energy, nevts.'
                        )
    args = parser.parse_args()

    # Execute the main function
    main(**vars(args))
