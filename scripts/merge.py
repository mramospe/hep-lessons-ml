#!/usr/bin/env python
'''
Merge CSV files into a single file.
'''
import argparse
import logging
import os
import pandas

logger = logging.getLogger()


def main(ofile, files):

    logger.info(f'Merging {len(files)} files into "{ofile}"')

    if os.path.isfile(ofile):
        raise RuntimeError(f'Output file "{ofile}" already exists')

    # can not update using a compressed file using "to_csv"
    logger.info(f'- {files[0]}')
    df = pandas.read_hdf(files[0], key='root')
    ievt = df['event'][len(df) - 1] + 1
    df.to_hdf(ofile, mode='w', key='root', format='t',
              index=False)  # this writes the column names
    for f in files[1:]:
        logger.info(f'- {f}')
        df = pandas.read_hdf(f, key='root')
        df['event'] += ievt
        df.to_hdf(ofile, mode='a', key='root', append=True, format='t',
                  index=False)  # do not write the column names
        ievt = df['event'][len(df) - 1] + 1


if __name__ == '__main__':

    logging.basicConfig(
        format='%(levelname)s: %(message)s', level=logging.INFO)

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('ofile', type=str, default='merged.hdf5',
                        help='Name of the output file')
    parser.add_argument('files', nargs='*', help='Input files to merge')
    args = parser.parse_args()
    main(**vars(args))
