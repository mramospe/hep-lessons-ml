#!/usr/bin/env python
'''
Setup script for the "hep-lessons-ml" package
'''

__author__ = 'Miguel Ramos Pernas'
__email__ = 'miguel.ramos.pernas@cern.ch'

import os
import shutil
import sys
import urllib.request
from setuptools import setup, find_packages


def download(url, where):
    '''
    Download a sample from CernBox.
    '''
    if os.path.exists(where):
        if not os.path.isfile(where):
            raise FileExistsError(f'Path "{where}" exists and is not a file')
        else:
            return
    
    where_dir = os.path.dirname(where)

    if os.path.exists(where_dir):
        if not os.path.isdir(where_dir):
            raise FileExistsError(f'Path "{where_dir}" exists and does not point to a directory')
    else:
        os.makedirs(where_dir) # create a directory to put the samples
    
    with urllib.request.urlopen(url) as response, open(where, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)


pwd = os.path.abspath(os.path.dirname(__file__))

sample_path = lambda s: os.path.join(pwd, 'hep_lessons_ml', 'samples', s)

files = {
    sample_path('reco_pp_450.0_400000.hdf5'): 'https://cernbox.cern.ch/index.php/s/ITytZAD7mMneNj1/download',
    sample_path('data.hdf5'): 'https://cernbox.cern.ch/index.php/s/ys319P8XQp6vp5R/download',
    sample_path('k0s2pi2.hdf5'): 'https://cernbox.cern.ch/index.php/s/fqebtiOyVBH6OyN/download',
    sample_path('k0s2pimunu.hdf5'): 'https://cernbox.cern.ch/index.php/s/MIdUb2Z0AuGMLSc/download',
    }

print('Processing samples (download if necessary)')
for path, url in files.items():
    print(f'- source: "{url}"; path: "{path}"')
    download(url, path)

notebooks = [os.path.join(root, f) for root, _, files in os.walk(os.path.join(pwd, 'hep_lessons_ml', 'notebooks')) for f in files]

with open(os.path.join(pwd, 'requirements.txt')) as f:
    requirements = f.read().split()

# Setup function
setup(

    name='hep_lessons_ml',

    description='Lessons on Machine Learning for High Energy Physics',

    # Read the long description from the README
    long_description=open('README.rst').read(),

    # Keywords to search for the package
    keywords='lessons hep high energy physics machine learning',

    # Data files
    package_data={'hep_lessons_ml': notebooks + list(files.keys())},

    # Find all the packages in this directory
    packages=find_packages(),

    # Install scripts
    scripts=['scripts/hep-lessons-ml'],

    # Requirements
    install_requires=requirements,
)
